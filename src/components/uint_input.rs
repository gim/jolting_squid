use web_sys::HtmlInputElement;
use yew::events::FocusEvent;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone)]
pub struct UintInputProps {
    pub label: String,
    pub value: u64,
    pub on_change: Callback<u64>,
}

#[function_component(UintInput)]
pub fn text_input(props: &UintInputProps) -> Html {
    let edit_state = use_state_eq(|| false);
    let edit_success = use_state_eq(|| true);

    let onblur = {
        let on_change = props.on_change.clone();
        let edit_state = edit_state.clone();
        let edit_success = edit_success.clone();

        move |e: FocusEvent| {
            let input: HtmlInputElement = e.target_unchecked_into();

            match input.value().parse::<u64>() {
                Ok(new_value) => {
                    edit_state.set(false);
                    edit_success.set(true);
                    on_change.emit(new_value);
                }
                _ => {
                    edit_success.set(false);
                    edit_state.set(false);
                }
            }
        }
    };

    let onfocusin = {
        let edit_state = edit_state.clone();

        move |_| edit_state.clone().set(true)
    };

    html! {
        <div
            class="text_input">
            <label>{&props.label}</label>
            <input
                type="text"
                value={props.value.to_string()}
                readonly={!*edit_state.clone()}
                {onblur}
                {onfocusin}
            />
        </div>
    }
}
