use super::uint_input::UintInput;
use crate::character_sheet::Attributes;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone)]
pub struct AttributesControlProps {
    pub attributes: Attributes,
    pub on_change: Callback<Attributes>,
}

#[function_component(AttributesControl)]
pub fn attributes_control(props: &AttributesControlProps) -> Html {
    html! {
        <div
            class="char_attributes">
            <UintInput
                label="Body:"
                value={props.attributes.body}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            body: value,
                            ..props.attributes
                })})}
            />
            <UintInput
                label="Agility:"
                value={props.attributes.agility}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            agility: value,
                            ..props.attributes
                })})}
            />
            <UintInput
                label="Reaction:"
                value={props.attributes.reaction}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            reaction: value,
                            ..props.attributes
                })})}
            />
            <UintInput
                label="Strength:"
                value={props.attributes.strength}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            strength: value,
                            ..props.attributes
                })})}
            />
            <UintInput
                label="Willpower:"
                value={props.attributes.willpower}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                    props.on_change.emit(Attributes{
                        willpower: value,
                        ..props.attributes
                })})}
            />
            <UintInput
                label="Logic:"
                value={props.attributes.logic}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            logic: value,
                            ..props.attributes
                })})}
            />
            <UintInput
                label="Intuition:"
                value={props.attributes.intuition}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            intuition: value,
                            ..props.attributes
                })})}
            />
            <UintInput
                label="Charisma:"
                value={props.attributes.charisma}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            charisma: value,
                            ..props.attributes
                    })})}
            />
            <UintInput
                label="Essence:"
                value={props.attributes.essence}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                    props.on_change.emit(Attributes{
                        essence: value,
                        ..props.attributes
                    })})}
            />
            <UintInput
                label="Edge:"
                value={props.attributes.edge}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            edge: value,
                            ..props.attributes
                    })})}
            />
            <UintInput
                label="Magic:"
                value={props.attributes.magic}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            magic: value,
                            ..props.attributes
                        })})}
            />
            <UintInput
                label="Resonance:"
                value={props.attributes.resonance}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: u64| {
                        props.on_change.emit(Attributes{
                            resonance
                            : value,
                            ..props.attributes
                    })})}
            />
        </div>
    }
}
