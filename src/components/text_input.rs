use web_sys::HtmlInputElement;
use yew::events::FocusEvent;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone)]
pub struct TextInputProps {
    pub label: String,
    pub value: String,
    pub on_change: Callback<String>,
}

#[function_component(TextInput)]
pub fn text_input(props: &TextInputProps) -> Html {
    let edit_state = use_state_eq(|| false);

    let onblur = {
        let on_change = props.on_change.clone();
        let edit_state = edit_state.clone();

        move |e: FocusEvent| {
            let input: HtmlInputElement = e.target_unchecked_into();
            let new_value = input.value();
            edit_state.set(false);
            on_change.emit(new_value);
        }
    };

    let onfocusin = {
        let edit_state = edit_state.clone();

        move |_| edit_state.clone().set(true)
    };

    html! {
        <div
            class="text_input">
            <label>{&props.label}</label>
            <input
                type="text"
                value={props.value.clone()}
                readonly={!*edit_state.clone()}
                {onblur}
                {onfocusin}
            />
        </div>
    }
}
