use super::text_input::TextInput;
use crate::character_sheet::Identity;
use yew::prelude::*;

#[derive(PartialEq, Properties, Clone)]
pub struct IdentityControlProps {
    pub identity: Identity,
    pub on_change: Callback<Identity>,
}

#[function_component(IdentityControl)]
pub fn identity_control(props: &IdentityControlProps) -> Html {
    html! {
        <div class="identity">
            <TextInput
                label="Name:"
                value={props.identity.name.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            name: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Street Name:"
                value={props.identity.street_name.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            street_name: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Player Name:"
                value={props.identity.player_name.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            player_name: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Meta Type:"
                value={props.identity.meta_type.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            meta_type: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Age:"
                value={props.identity.age.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            age: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Gender:"
                value={props.identity.gender.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            gender: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Pronouns:"
                value={props.identity.pronouns.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            pronouns: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Height:"
                value={props.identity.height.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            height: value,
                            ..props.identity.clone()
                })})}
            />
            <TextInput
                label="Weight:"
                value={props.identity.weight.clone()}
                on_change={
                    let props = props.clone();
                    Callback::from(move |value: String| {
                        props.on_change.emit(Identity{
                            weight: value,
                            ..props.identity.clone()
                })})}
            />
        </div>
    }
}
