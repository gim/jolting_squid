use yew::prelude::*;

mod components;
use components::{attributes_control::AttributesControl, identity_control::IdentityControl};

mod character_sheet;
use character_sheet::{Attributes, Character, Identity};

enum Msg {
    ChangedIdentity(Identity),
    ChangedAttributes(Attributes),
}

struct Model {
    character: Character,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            character: Default::default(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::ChangedIdentity(identity) => {
                if self.character.identity != identity {
                    self.character.identity = identity;
                    true
                } else {
                    false
                }
            }
            Msg::ChangedAttributes(attributes) => {
                if self.character.attributes != attributes {
                    self.character.attributes = attributes;
                    true
                } else {
                    false
                }
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();

        html! {
            <>
                <IdentityControl
                    identity={self.character.identity.clone()}
                    on_change={link.callback(|identity| Msg::ChangedIdentity(identity))}
                />
                <AttributesControl
                    attributes={self.character.attributes.clone()}
                    on_change={link.callback(|attributes| Msg::ChangedAttributes(attributes))}
                />
            </>
        }
    }
}

fn main() {
    yew::start_app::<Model>();
}
