#[derive(Default, PartialEq, Clone)]
pub struct Character {
    pub identity: Identity,
    pub attributes: Attributes,
}

#[derive(Default, PartialEq, Clone)]
pub struct Identity {
    pub name: String,
    pub street_name: String,
    pub player_name: String,
    pub meta_type: String,
    pub age: String,
    pub gender: String,
    pub pronouns: String,
    pub height: String,
    pub weight: String,
}

#[derive(Default, PartialEq, Clone)]
pub struct Attributes {
    pub body: u64,
    pub agility: u64,
    pub reaction: u64,
    pub strength: u64,
    pub willpower: u64,
    pub logic: u64,
    pub intuition: u64,
    pub charisma: u64,
    pub essence: u64,
    pub edge: u64,
    pub magic: u64,
    pub resonance: u64,
}
